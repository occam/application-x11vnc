apt-get install -y xdm xserver-xephyr xvfb xpra autoconf
apt-get install -y libx11-dev libxtst-dev libXext-dev libXinerama-dev libjpeg-dev libXdamage-dev zlib1g-dev libpng-dev

mkdir local

cd libvncserver
autoreconf -vfi
./configure --prefix=`pwd`/../local
make
make install
cd ..

cd x11vnc
autoreconf -vfi
PKG_CONFIG_PATH=`pwd`/../libvncserver ./configure --prefix=`pwd`/../local
make
make install
cd ..
